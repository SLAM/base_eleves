<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Pdo;

use App\Bdd\Config;
use \PDO;
use App\Models\Classe;
use App\Models\Prof;
use App\Models\Eleve;
use App\Models\Specialite;

/**
 * @brief Classe d'accès aux données.
 *
 * Utilise les services de la classe PDO pour l'application.
 * Les attributs sont tous statiques :
 * * $monPdo de type PDO
 * * $monPdoPass qui contiendra l'unique instance de la classe
 */
class Passerelle
{
    /**
     * @var PDO $monPdo
     * Instance de la classe PDO.
     */
    private static $monPdo;

    /**
     * @var Passerelle $monPdoPass
     * Unique instance de la classe Passerelle.
     */
    private static $monPdoPass = null;
    
    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct()
    {
        Passerelle::$monPdo = new PDO(Config::DATABASE_URI, Config::USER, Config::PASSWD);
        Passerelle::$monPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Destructeur de la classe
     */
    public function __destruct()
    {
        Passerelle::$monPdo = null;
    }

    /**
     * Fonction statique qui crée l'unique instance de la classe
     *
     * @code
     *   $instancePasserelle = Passerelle::getPdoPass(); // Appel
     * @endcode
     *
     * @return Passerelle Unique objet de la classe
     */
    public static function getPdoPass(): Passerelle
    {
        if (Passerelle::$monPdoPass == null) {
            Passerelle::$monPdoPass = new Passerelle();
        }
        return Passerelle::$monPdoPass;
    }

    /**
     * Retourne une collection d'objets Classe
     *
     * @return array Collection d'objets Classe ou null (si aucun enregistrement n'est trouvé)
     */
    public function obtenirClasses(): ?array
    {
        $req = "SELECT * FROM classes
                ORDER BY id_class";
        $res = Passerelle::$monPdo->query($req);
        $result = $res->fetchAll();

        if ($result) {
            $classes = array();
            foreach ($result as $ligne) {
                $idClasse = intval($ligne['id_class']);
                $uneClasse = $this->obtenirDetailClasse($idClasse);
                $classes[$idClasse] = $uneClasse;
            }
            return $classes;
        }
        return null;
    }

    /**
     * Retourne les informations sur une classe
     *
     * @param int $unId Identifiant de la classe
     * @return Classe Objet Classe ou null (si aucun enregistrement n'est trouvé)
     */
    public function obtenirDetailClasse(int $unId): ?Classe
    {
        $req = "SELECT classes.*,  specialites.*,
                p1.id_p AS elev_id, p1.nom AS elev_nom, p1.prenom AS elev_prenom,
                p2.id_p AS prof_id, p2.nom AS prof_nom, p2.prenom AS prof_prenom,
                eleves.tel_resp AS elev_resp FROM classes
                JOIN profs ON profs.id_prof = classes.prof_principal
                JOIN eleves ON eleves.ref_class = classes.id_class
                JOIN personnes AS p1 ON p1.id_p = eleves.id_elev
                JOIN personnes AS p2 ON p2.id_p = profs.id_prof
                JOIN specialites ON specialites.id_spe = profs.ref_spe
                WHERE id_class = :un_id
                ORDER BY p1.nom";
        $st = Passerelle::$monPdo->prepare($req);
        $st->bindValue(':un_id', $unId, PDO::PARAM_INT);
        $res = $st->execute();
        $result = $st->fetchAll();

        if ($result) {
            $prems = true;
            $classe = new Classe();
            $classe->setId($unId);
            $profPrincipal = new Prof();
            $eleves = array();
            foreach ($result as $ligne) {
                if ($prems) {
                    $prems = false;
                    $classe->setLibelle($ligne['lib_class']);
                    $profPrincipal->setId(intval($ligne['prof_id']));
                    $profPrincipal->setNom($ligne['prof_nom']);
                    $profPrincipal->setPrenom($ligne['prof_prenom']);
                    $profPrincipal->setSpecialite(new Specialite(
                        intval($ligne['id_spe']),
                        $ligne['lib_spe']
                    ));
                }
                $idElev = intval($ligne['elev_id']);
                $unEleve = new Eleve(
                    $idElev,
                    $ligne['elev_nom'],
                    $ligne['elev_prenom'],
                    $ligne['elev_resp']
                );
                $eleves[$idElev] = $unEleve;
            }
            $classe->setProfPrincipal($profPrincipal);
            $classe->setLesEleves($eleves);
            return $classe;
        }
        return null;
    }

    /**
     * Retourne une collection d'objets Eleve
     *
     * @return array Collection d'objets Eleve ou null (si aucun enregistrement n'est trouvé)
     */
    public function obtenirEleves(): ?array
    {
        // TODO
        return null;
    }

    /**
     * Retourne les informations sur un élève
     *
     * @param int $unId Identifiant de l'élève
     * @return Eleve Instance de la classe Eleve ou null (si aucun enregistrement n'est trouvé)
     */
    public function obtenirDetailEleve(int $unId): ?Eleve
    {
        // TODO
        return null;
    }
}
