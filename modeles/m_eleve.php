<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Models;

/**
 * Définition de la classe métier représentant un élève.
 */
class Eleve extends Personne
{
    /**
     * @var string $telResp
     * Téléphone du responsable de l'élève
     */
    private $telResp;

    /**
     * Constructeur de la classe
     */
    public function __construct(int $unId, string $unNom, string $unPrenom, string $unTelResp = null)
    {
        parent::__construct($unId, $unNom, $unPrenom);
        if ($unTelResp) {
            $this->setTelResp($unTelResp);
        }
    }

    /**
     * Accesseur du téléphone
     * @return string Téléphone du responsable de l'élève
     */
    public function getTelResp(): ?string
    {
        return $this->telResp;
    }

    /**
     * Mutateur du téléphone
     * @param string $unTelResp Téléphone du responsable de l'élève
     */
    public function setTelResp(string $unTelResp)
    {
        $this->telResp = $unTelResp;
    }
}
