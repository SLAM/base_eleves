<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Models;

/**
 * Définition de la classe métier représentant une classe.
 */
class Classe
{
    /**
     * @var int $id
     * Identifiant de la classe
     */
    private $id;
    
    /**
     * @var string $libelle
     * Libellé de la classe
     */
    private $libelle;

    /**
     * @var Prof $profPrincipal
     * Professeur principal de la classe
     */
    private $profPrincipal;

    /**
     * @var array $lesEleves
     * Les élèves de la classe
     */
    private $lesEleves;

    /**
     * Constructeur de la classe
     */
    public function __construct()
    {
        $this->lesEleves = [];
    }
    
    /**
     * Accesseur de l'identifiant
     * @return int Identifiant de la classe
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Accesseur du libellé
     * @return string Libellé de la classe
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * Accesseur du professeur principal
     * @return Prof Professeur principal de la classe
     */
    public function getProfPrincipal(): Prof
    {
        return $this->profPrincipal;
    }

    /**
     * Accesseur des élèves
     * @return array Collection d'instances de la classe Eleve
     */
    public function getLesEleves(): array
    {
        return $this->lesEleves;
    }

    /**
     * Mutateur de l'identifiant
     * @param int $unId Identifiant de la classe
     */
    public function setId(int $unId)
    {
        $this->id = $unId;
    }

    /**
     * Mutateur du libellé
     * @param string $unLibelle Libellé de la classe
     */
    public function setLibelle(string $unLibelle)
    {
        $this->libelle = $unLibelle;
    }

    /**
     * Mutateur du professeur principal
     * @param Prof $unProf Professeur principal de la classe
     */
    public function setProfPrincipal(Prof $unProf)
    {
        $this->profPrincipal = $unProf;
    }

    /**
     * Mutateur des élèves
     * @param array $desEleves Collection d'instances de la classe Eleve
     */
    public function setLesEleves(array $desEleves)
    {
        $this->lesEleves = $desEleves;
    }
}
