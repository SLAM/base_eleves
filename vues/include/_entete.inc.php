    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Base élèves - <?php echo $titre; ?></title>
    <link href="./vues/include/styles/bootstrap.min.css"
        rel="stylesheet" type="text/css" media="screen">
    <link href="./vues/include/styles/bootstrap-amap.css"
        rel="stylesheet" type="text/css" media="screen">
    <script src="./vues/include/js/jquery.min.js"></script>
    <script src="./vues/include/js/bootstrap.min.js"></script>
    <link rel="icon" href="./vues/include/images/favicon.ico">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js">
    </script>
    <![endif]-->
