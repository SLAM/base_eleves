<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <?php include("vues/include/_erreurs.php"); ?>
        <div class="panel panel-amap">
            <div class="panel-heading text-center">
                <strong><?php echo $titre; ?></strong>
            </div>
            <?php if ($lesClasses) : ?>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <td><strong>Libellé</strong></td>
                        <td><strong>Nom prof. principal</strong></td>
                        <td><strong>Prénom prof. principal</strong></td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($lesClasses as $c) : ?>
                    <tr>
                        <td>
                            <a href="index.php?uc=gererClasses&action=detail&id=<?php echo $c->getId(); ?>"
                               title="Voir détail">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <span class="hidden-xs"><?php echo $c->getLibelle(); ?></span>
                            </a>
                        </td>
                        <td><?php echo $c->getProfPrincipal()->getNom(); ?></td>
                        <td><?php echo $c->getProfPrincipal()->getPrenom(); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php else : ?>
            <p>Il n'y a pas de classes enregistrées.</p>
            <?php endif; ?>
            <div class="panel-footer">
                <a href="index.php?uc=gererClasses&action=creation">
                    <span class="glyphicon glyphicon-plus-sign"></span>
                    Création d'une nouvelle classe
                </a>
            </div>
        </div>
    </div>
  </body>
</html>
