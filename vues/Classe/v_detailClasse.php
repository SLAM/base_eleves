<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <?php include("vues/include/_erreurs.php"); ?>
        <div class="panel panel-amap">
            <div class="panel-heading text-center">
                <strong><?php echo $titre; ?></strong>
                (prof principal: <?php echo $maClasse->getProfPrincipal()->getNom(); ?>)
            </div>
            <?php if ($maClasse->getLesEleves()) : ?>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <td><strong>Nom élève</strong></td>
                        <td><strong>Prénom élève</strong></td>
                        <td><strong>Tel. responsable</strong></td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($maClasse->getLesEleves() as $e) : ?>
                    <tr>
                        <td><?php echo $e->getNom(); ?></td>
                        <td><?php echo $e->getPrenom(); ?></td>
                        <td><?php echo $e->getTelResp(); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php else : ?>
            <p>Il n'y a pas d'élèves enregistrés.</p>
            <?php endif; ?>
            <div class="panel-footer">
                <a href="index.php?uc=gererClasses">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span>
                    Retour
                </a>
            </div>
        </div>
    </div>
  </body>
</html>
