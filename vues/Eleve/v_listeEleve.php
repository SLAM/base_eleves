<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <?php include("vues/include/_erreurs.php"); ?>
        <div class="panel panel-amap">
            <div class="panel-heading text-center">
                <strong>Liste des élèves</strong>
            </div>
            <?php if ($lesEleves) : ?>
            <table class="table table-responsive table-hover">
                <thead>
                    <tr>
                        <td><strong>Identifiant</strong></td>
                        <td><strong>Nom</strong></td>
                        <td><strong>Prénom</strong></td>
                        <td><strong>Classe</strong></td>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($lesEleves as $e) : ?>
                    <tr>
                        <td>
                            <a href="index.php?uc=gererEleves&action=detail&id=<?php echo $e->getId(); ?>"
                               title="Voir détail">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <span class="hidden-xs"><?php echo $e->getId(); ?></span>
                            </a>
                        </td>
                        <td><?php echo $e->getNom(); ?></td>
                        <td><?php echo $e->getPrenom(); ?></td>
                        <td>&nbsp;</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php else : ?>
            <p>Il n'y a pas d'élèves enregistrés.</p>
            <?php endif; ?>
            <div class="panel-footer">
                <a href="index.php?uc=gererEleves&action=creation">
                    <span class="glyphicon glyphicon-plus-sign"></span>
                    Création d'un nouvel élève
                </a>
            </div>
        </div>
    </div>
  </body>
</html>
