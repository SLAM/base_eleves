<?php
include('class.config.inc.php');
use App\Bdd\Config;

$db = new PDO(Config::DATABASE_URI, Config::USER, Config::PASSWD);
$sql = file_get_contents(Config::SCRIPT);
$qr = $db->exec($sql);
