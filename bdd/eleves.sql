DROP TABLE IF EXISTS eleves;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS profs;
DROP TABLE IF EXISTS specialites;
DROP TABLE IF EXISTS personnes;

-- Création des tables
CREATE TABLE personnes
(id_p INTEGER NOT NULL,
nom VARCHAR(45) NOT NULL,
prenom VARCHAR(45) NOT NULL,
CONSTRAINT pk_personnes PRIMARY KEY(id_p));

CREATE TABLE specialites
(id_spe INTEGER NOT NULL,
lib_spe VARCHAR(25) NOT NULL,
CONSTRAINT pk_specialites PRIMARY KEY(id_spe));

CREATE TABLE profs
(id_prof INTEGER NOT NULL,
ref_spe INTEGER NOT NULL,
CONSTRAINT pk_profs PRIMARY KEY(id_prof),
CONSTRAINT fk_profs_1 FOREIGN KEY(id_prof) REFERENCES personnes(id_p),
CONSTRAINT fk_profs_2 FOREIGN KEY(ref_spe) REFERENCES specialites(id_spe));

CREATE TABLE classes
(id_class INTEGER NOT NULL,
lib_class VARCHAR(10) NOT NULL,
prof_principal INTEGER NOT NULL,
CONSTRAINT pk_classes PRIMARY KEY(id_class),
CONSTRAINT fk_classes FOREIGN KEY(prof_principal) REFERENCES profs(id_prof));

CREATE TABLE eleves
(id_elev INTEGER NOT NULL,
tel_resp VARCHAR(10) DEFAULT NULL,
ref_class INTEGER NOT NULL,
CONSTRAINT pk_eleves PRIMARY KEY(id_elev),
CONSTRAINT fk_eleves_1 FOREIGN KEY(id_elev) REFERENCES personnes(id_p),
CONSTRAINT fk_eleves_2 FOREIGN KEY(ref_class) REFERENCES classes(id_class));

-- Les personnes sont fictives
INSERT INTO personnes VALUES (1, 'Moulinux', 'Stéphane');
INSERT INTO personnes VALUES (2, 'Salazux', 'Sabine');
INSERT INTO personnes VALUES (3, 'Tux', 'Michèle');
INSERT INTO personnes VALUES (4, 'Patamob', 'Adhémar');
INSERT INTO personnes VALUES (5, 'Zeublouze', 'Agathe');
INSERT INTO personnes VALUES (6, 'Kuzbidon', 'Alex');
INSERT INTO personnes VALUES (7, 'Locale', 'Anasthasie');
INSERT INTO personnes VALUES (8, 'Zoudanlkou', 'Debbie');

-- Les spécialités sont fictives
INSERT INTO specialites VALUES (1, 'SLAM');
INSERT INTO specialites VALUES (2, 'SISR');

-- Les profs sont fictifs
INSERT INTO profs VALUES (1, 1);
INSERT INTO profs VALUES (2, 2);

-- Les classes sont fictives
INSERT INTO classes VALUES (1, 'S1SIO', 2);
INSERT INTO classes VALUES (2, 'S2SIO', 1);

-- Les élèves sont fictifs
INSERT INTO eleves VALUES (3, '0203040506', 1);
INSERT INTO eleves VALUES (4, '0466040506', 2);
INSERT INTO eleves VALUES (5, '0477040506', 2);
INSERT INTO eleves VALUES (6, '0488040506', 2);
INSERT INTO eleves VALUES (7, '0499040506', 2);
INSERT INTO eleves VALUES (8, '0463040506', 2);
