<?php
/**
 * Contrôleur principal de l'application Web.
 *
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 *
 */
require_once("bdd/class.config.inc.php");
require_once("include/class.passerelle.inc.php");
require_once("include/fct.inc.php");
require_once("modeles/m_specialite.php");
require_once("modeles/m_personne.php");
require_once("modeles/m_classe.php");
require_once("modeles/m_eleve.php");
require_once("modeles/m_prof.php");
use App\Pdo\Passerelle;

$pdo = Passerelle::getPdoPass();
$uc = null;
if (isset($_REQUEST['uc'])) {
    $uc = $_REQUEST['uc'];
}
switch ($uc) {
    case 'gererClasses':
        include("controleurs/c_gererClasses.php");
        break;
    case 'gererEleves':
        include("controleurs/c_gererEleves.php");
        break;
    default:
        include("controleurs/c_accueil.php");
        break;
}
