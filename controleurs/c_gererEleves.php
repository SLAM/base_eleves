<?php
$action = null;
$id = null;
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
}
if (isset($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
}
switch ($action) {
    case 'detail':
        $monEleve = $pdo->obtenirDetailEleve(intval($id));
        if ($monEleve) {
            $titre = "Détail de l'élève n°".$id;
            $classAccueil = "";
            $classClasses = "";
            $classEleves = "active";
            include("vues/Eleve/v_detailEleve.php");
            break;
        }
        ajouterErreur("$id, n'est pas un identifiant d'élève connu dans la base");
        // pas de break, afin de basculer sur le traitement par défaut
    default:
        $titre = "Liste des élèves";
        $classAccueil = "";
        $classClasses = "";
        $classEleves = "active";
        $lesEleves = $pdo->obtenirEleves();
        include("vues/Eleve/v_listeEleve.php");
        break;
}
