Ce programme permet d'aborder les révisions en début de deuxième année.
L'application permet de gérer un système éducatif réduit.

Pour initialiser l'application dans un environnement de développement.

Positionnez vous dans votre répertoire de travail :
```
mkdir -p $HOME/prog
cd $HOME/prog
```

Clonez le projet :
```
git clone git@framagit.org:SLAM/base_eleves.git
```

Vous pouvez enfin créer une base locale sqlite, pour la déplacer ensuite dans le dossier racine :
```
cd base_eleves/bdd
php createDb.php
mv eleves.db ../.
cd ..
```

> Vous pouvez bien sûr configurer une autre base de données via le fichier
`bdd/class.config.inc.php`.

Enfin, vous lancez le serveur de développement de PHP :
```
php -S 127.0.0.1:8000
```

> Pré-requis sur le système. Sur une distribution GNU/Linux Debian, un
certain nombre de paquets devront être installés sur le système à l'aide
de la commande `apt -y install php-cli php-sqlite3 php-codesniffer`

> Le respect du style du code a été testé à l'aide la commande :
`phpcs --standard=PSR2 index.php`
